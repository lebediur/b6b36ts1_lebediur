import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {
    Calculator calc;

    @BeforeEach
    public void eachTestSetUp() {
        calc = new Calculator();
    }

    @Test
    public void Add_AddingOneAndOne_Two() {
        int expectedValue = 12;
        int res = calc.add(4, 8);
        assertEquals(expectedValue, res);
    }

    @Test
    public void Subtract_SubtractingTwoAndOne_One() {
        int expectedValue = 2;
        int res = calc.subtract(3, 1);
        assertEquals(expectedValue, res);
    }

    @Test
    public void Multiply_MultiplyingTwoByTwo_Four() {
        int expectedValue = 4;
        int res = calc.multiply(2, 2);
        assertEquals(expectedValue, res);
    }

    @Test
    public void Divide_DividingTenByTwo_Five() throws Exception {
        int expectedValue = 1;
        int res = calc.divide(2, 2);
        assertEquals(expectedValue, res);
    }

    @Test
    public void Divide_DividingTwoByZero_Exception() throws Exception {
        Assertions.assertThrows(Exception.class, () -> {
            calc.divide(2, 0);
        });
    }

    @ParameterizedTest(name="{0} plus {1} should be eqyal to {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5"})
    public void add_addAandB_returnC(int a, int b, int c) {
        int expectedResult = c;
        int result = calc.add(a, b);

        assertEquals(expectedResult, result);
    }
}
